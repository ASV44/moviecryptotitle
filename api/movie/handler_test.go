package movie

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/suite"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"api/protobuf"
	"api/protobuf/mocks"
)

type handlerTestSuite struct {
	suite.Suite
	mockCrypto *mocks.CryptoClient
	handler    Handler
}

func (s *handlerTestSuite) SetupTest() {
	s.mockCrypto = new(mocks.CryptoClient)
	s.handler = NewHandler(s.mockCrypto)
}

func TestClientSuite(t *testing.T) {
	suite.Run(t, new(handlerTestSuite))
}

func (s *handlerTestSuite) Test_HashMovie_Success() {
	recorder := httptest.NewRecorder()
	movie := Movie{Title: "some-title"}
	reqData, err := json.Marshal(movie)
	s.NoError(err)
	req := httptest.NewRequest(http.MethodPost, "/hash-movie-name", bytes.NewReader(reqData))
	expectedHash := "some-expected-hash"
	s.mockCrypto.On("Hash", req.Context(), &protobuf.Data{Data: movie.Title}).
		Return(&protobuf.DataHash{Hash: expectedHash}, nil)
	expectedResp := HashResponse{TitleHash: expectedHash}

	s.handler.HashMovie(recorder, req)

	result := recorder.Result()
	var resp HashResponse
	s.NoError(json.NewDecoder(result.Body).Decode(&resp))
	s.Equal(expectedResp, resp)
}

func (s *handlerTestSuite) Test_HashMovie_InvalidTitle() {
	recorder := httptest.NewRecorder()
	movie := Movie{Title: "some-title"}
	reqData, err := json.Marshal(movie)
	s.NoError(err)
	req := httptest.NewRequest(http.MethodPost, "/hash-movie-name", bytes.NewReader(reqData))
	expectedErrMsg := "some error message"
	s.mockCrypto.On("Hash", req.Context(), &protobuf.Data{Data: movie.Title}).
		Return(nil, status.Error(codes.InvalidArgument, expectedErrMsg))
	expectedResp := Error{Title: movie.Title, Error: "unsupported movie provided", Description: expectedErrMsg}

	s.handler.HashMovie(recorder, req)

	result := recorder.Result()
	var resp Error
	s.NoError(json.NewDecoder(result.Body).Decode(&resp))
	s.Equal(expectedResp, resp)
}
