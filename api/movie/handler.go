package movie

import (
	"encoding/json"
	"log/slog"
	"net/http"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"api/protobuf"
)

type Handler struct {
	crypto protobuf.CryptoClient
}

func NewHandler(crypto protobuf.CryptoClient) Handler {
	return Handler{crypto: crypto}
}

func (h Handler) HashMovie(w http.ResponseWriter, req *http.Request) {
	var movie Movie
	if err := json.NewDecoder(req.Body).Decode(&movie); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	dataHash, err := h.crypto.Hash(req.Context(), &protobuf.Data{Data: movie.Title})
	if err != nil {
		respStatus := status.Convert(err)
		if respStatus.Code() == codes.InvalidArgument {
			slog.Error("invalid movie provided", "err", err)

			resp, err := json.Marshal(Error{Title: movie.Title, Error: "unsupported movie provided", Description: respStatus.Message()})
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(http.StatusBadRequest)
			w.Write(resp)

			return
		}

		slog.Error("failed to perform gRPC call to crypto service", "err", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	resp, err := json.Marshal(HashResponse{TitleHash: dataHash.Hash})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(resp)
}
