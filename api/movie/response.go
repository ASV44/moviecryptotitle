package movie

type HashResponse struct {
	TitleHash string `json:"titleHash"`
}

type Error struct {
	Title       string `json:"title"`
	Error       string `json:"error"`
	Description string `json:"description"`
}
