package router

import (
	"net/http"

	"github.com/gorilla/mux"

	"api/movie"
)

func New(handler movie.Handler) *mux.Router {
	router := mux.NewRouter()

	router.Path("/health").
		Methods(http.MethodGet).
		HandlerFunc(Health).
		Name("health")

	router.Path("/hash-movie-name").
		Methods(http.MethodPost).
		HandlerFunc(handler.HashMovie).
		Name("root")

	return router
}

// Health returns 200 OK for every request
func Health(w http.ResponseWriter, _ *http.Request) {
	w.WriteHeader(http.StatusOK)
}
