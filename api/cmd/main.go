package main

import (
	"context"
	"errors"
	"log/slog"
	"net/http"
	"os"
	"os/signal"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"api/movie"
	"api/protobuf"
	"api/router"
)

const defaultTimeout = 10 * time.Second

func main() {
	conn, err := grpc.Dial(os.Getenv("CRYPTO_SERVICE_ADDR"), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		slog.Error("failed to dial crypto service", "err", err)
		return
	}

	crypto := protobuf.NewCryptoClient(conn)
	movieReqHandler := movie.NewHandler(crypto)

	server := http.Server{
		Addr:         os.Getenv("PORT"),
		Handler:      router.New(movieReqHandler),
		ReadTimeout:  defaultTimeout,
		WriteTimeout: defaultTimeout,
	}

	// Start server
	go func() {
		if err = server.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			slog.Error("shutting down the server", "err", err)
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with a timeout of 10 seconds.
	// Use a buffered channel to avoid missing signals as recommended for signal.Notify
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), defaultTimeout)
	defer cancel()
	if err := server.Shutdown(ctx); err != nil {
		slog.Error("failed to shutdown the server", "err", err)
	}
}
