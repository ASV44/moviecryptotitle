package crypto

import (
	"context"
	"crypto/sha256"
	"fmt"
	"log/slog"
	"strings"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"crypto/protobuf"
)

// Crypto represents service which handles crypto gRPC requests
type Crypto struct {
	protobuf.UnimplementedCryptoServer
}

func New() Crypto {
	return Crypto{}
}

func (c Crypto) Hash(_ context.Context, data *protobuf.Data) (*protobuf.DataHash, error) {
	reqData := strings.ToLower(data.GetData())
	if strings.Contains(reqData, "fast") || strings.Contains(reqData, "furious") {
		return nil, status.Error(codes.InvalidArgument, "dummy movies not supported")
	}

	sum := sha256.Sum256([]byte(reqData))
	dataHash := fmt.Sprintf("%x", sum)

	slog.Info("computed hash", "data", reqData, "hash", dataHash)

	return &protobuf.DataHash{Hash: dataHash}, nil
}
