package crypto

import (
	"context"
	"crypto/sha256"
	"fmt"
	"net"
	"testing"

	"github.com/stretchr/testify/suite"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"
	"google.golang.org/grpc/test/bufconn"

	"crypto/protobuf"
)

type cryptoTestSuite struct {
	suite.Suite
	server *grpc.Server
	client protobuf.CryptoClient
}

func (s *cryptoTestSuite) SetupSuite() {
	lis := bufconn.Listen(1024 * 1024)

	s.server = grpc.NewServer()
	protobuf.RegisterCryptoServer(s.server, New())
	go func() {
		s.NoError(s.server.Serve(lis))
	}()

	conn, err := grpc.Dial(
		"",
		grpc.WithContextDialer(func(context.Context, string) (net.Conn, error) {
			return lis.Dial()
		}),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	s.NoError(err)

	s.client = protobuf.NewCryptoClient(conn)
}

func (s *cryptoTestSuite) TearDownSuite() {
	s.server.Stop()
}

func TestClientSuite(t *testing.T) {
	suite.Run(t, new(cryptoTestSuite))
}

func (s *cryptoTestSuite) Test_Hash_Success() {
	data := "sample-data"
	sum := sha256.Sum256([]byte(data))
	expectedHash := fmt.Sprintf("%x", sum)

	hash, err := s.client.Hash(context.Background(), &protobuf.Data{Data: data})

	s.NoError(err)
	s.Equal(expectedHash, hash.Hash)
}

func (s *cryptoTestSuite) Test_Hash_InvalidDataProvided() {
	data := "Fast and Furious"
	expectedError := status.Error(codes.InvalidArgument, "dummy movies not supported")

	hash, err := s.client.Hash(context.Background(), &protobuf.Data{Data: data})

	s.ErrorIs(err, expectedError)
	s.Nil(hash)
}
