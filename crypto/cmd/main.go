package main

import (
	"log/slog"
	"net"
	"os"
	"os/signal"

	"google.golang.org/grpc"

	"crypto/crypto"
	"crypto/protobuf"
)

func main() {
	listener, err := net.Listen("tcp", os.Getenv("PORT"))
	if err != nil {
		slog.Error("failed to listen gRPC server", "err", err)
		return
	}

	server := grpc.NewServer()
	protobuf.RegisterCryptoServer(server, crypto.New())

	if err = server.Serve(listener); err != nil {
		slog.Error("gRPC server error", "err", err)
		return
	}

	// Wait for interrupt signal to gracefully shutdown the server with a timeout of 10 seconds.
	// Use a buffered channel to avoid missing signals as recommended for signal.Notify
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	server.GracefulStop()
}
