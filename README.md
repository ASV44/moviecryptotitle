# Movie Crypto Title

Sample project which computes SHA256 hash of provided movie title.

## Build and Deploy

In order to run project on local environment use`docker-compose`.

1. From project root run `docker-compose up`
2. Perform `POST` request to `localhos:8000`

Request Payload
```JSON
{
  "title": "Dark Knight"
}
```

## Components:
1. `api` service which represent REST `API`
2. `crypto` gRPC service which computes hash  of provided data
